$(document).ready(function(){

  ///////////////////
  ///draggable///////
  ///////////////////
  $(".container").sortable();
  $(".container").disableSelection();
  findSibling('.small_txt', 't');
  findSibling('img', 'i');
  findSibling('span', 's');
  findSibling('.work', 'w');
  $('.big_txt').each(function() {
    lineWrap(this);
  });
  placeObj('.small_txt');
  placeObj('img');
  placeObj('.work');
  placeObj('span');



  $(".elem").mousedown(function() {
    $(this).css("cursor", "grabbing");
    $(this).css("text-decoration", "underline");
  });
  $(".elem").mouseup(function() {
    $("div").css("cursor", "grab");
    $("div").css("text-decoration", "none");
  });


});






function lineWrap(obj) {
  var $cont = $(obj);
  var letter = $cont.html().split('');
  var cla = $cont.attr("class");
  for (i = 0; i < letter.length; i++) {
    if (letter[i] === " ") {
      letter[i] = '<div class="' + cla + '">' + "&nbsp;" + ' </div>';
    } else {
      letter[i] = '<div class="' + cla + '">' + letter[i] + ' </div>';
    }
  }
  //put the span into the container
  $cont.hide();
  $(".container").append(letter.join(''));
}

function findSibling(obj, letter) {
  var i = 0;
  $(obj).each(function() {
    i++;
    n = letter + i.toString();
    //faire un data et pas un class
    $(this).addClass(n);
    prev_elem = $(this)[0].previousElementSibling;
    $(prev_elem).addClass(n);
  });
}

function placeObj(obj) {
  $(obj).each(function() {
    var elems_b = [];
    var classes = $(this)[0].classList;
    var n = classes[classes.length - 1];
    $('.elem').each(function() {
      var elem = this;
      var classes_big = $(this)[0].classList;
      var n_b = classes_big[classes_big.length - 1];
      if (n_b === n) {
        elems_b.push(elem);
      }
    });
    $(this).insertAfter($(elems_b[elems_b.length - 1]))
  });
}


function goBack() {
    window.history.back();
}
function topFunction() {
  $('html,body').animate({ scrollTop: 0 }, 400);
  return false;
}
