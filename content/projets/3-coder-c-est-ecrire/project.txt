Title: Coder, c'est écrire

----

Type: Atelier

----

Year: Juin 2017

----

Place: Assises Internationales du Roman

----

City: Lyon

----

Text: Au delà des mots le travail du web implique une part importante de collage, ou de “copier/collage” pour être exact. Pour familiariser le grand public avec cette pratique d'emprunt permanent, Bonjour Monde a proposé à l'occasion des Assises Internationales du Roman 2017 un exercice d'écriture sans clavier : la création de textes (biographie, fable, poème, haïku, etc.) uniquement à coup de copier/coller et à partir d'une source unique, Wikipédia, encyclopédie en ligne d’une richesse (presque) inépuisable et touchant à (presque) tous les sujets. Après avoir façonné leur texte, en on modifié l'aspect, l'ont fait grossir, ou vibrer, et ce à l'aide de petits morceaux de code très simples !

----

Links: 

→ [Résultats](coderEcrire/index.php)

→ [AIR 2017](http://www.villagillet.net/portail/air/details/article/retour-sur-les-assises-internationales-du-roman-2017/)

----

Who: 

----

Copyright: 