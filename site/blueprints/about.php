<?php if(!defined('KIRBY')) exit ?>

title: About
pages: false
files: false
icon: truck
fields:
  title:
    label: Title
    type:  text
  texttop:
    label: Texte
    type:  wysiwyg
  textleft:
    label: Texte gauche
    type:  wysiwyg
    width:  1/3
  textmiddle:
    label: Texte milieu
    type:  wysiwyg
    width:  1/3
  textright:
    label: Texte droite
    type:  wysiwyg
    width:  1/3
