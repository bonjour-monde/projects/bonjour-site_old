<!DOCTYPE html>
<html class="no-js" lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="<?php echo $site->description() ?>">
    <meta name="keywords" content="<?php echo $site->keywords() ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>
        <?php echo $site->title() ?>
    </title>
    <link rel="shortcut icon" href="https://image.ibb.co/m5J5S6/favicon_01.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

    <?php echo css("assets/css/myswag.css") ?>
    <?php echo css("assets/fonts/font-awesome/css/font-awesome.min.css") ?>



<script>[].forEach.call(document.querySelectorAll('img[data-src]'),    function(img) {
  img.setAttribute('src', img.getAttribute('data-src'));
  img.onload = function() {
    img.removeAttribute('data-src');
  };});
</script>


</head>


<body class="loading" id="top">
    <div id="container" class="cf">
