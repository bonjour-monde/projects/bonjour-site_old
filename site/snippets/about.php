<div id="about-container">
    <div id="about-intro">
        <div class="menu-left">

        </div>
        <div class="menu-right">
            <a href="#" onclick="aboutclose();"><span class="about-close">Close</span></a>
        </div>
        <div id="about-text">
           <?php echo $page->texttop()->kirbytext()?>
            <br/>Want to get in touch? You can contact me by <a href="contact [at] lucasdescroix.fr"
   rel="nofollow"
   onclick="this.href='mailto:' + 'contact' + '@' + 'lucasdescroix.fr'; ">Mail</a>, on <a href="https://twitter.com/LucasDescroix">Twitter</a> or <a href="https://www.instagram.com/lucasdescroix/">Instagram</a>, I'd be happy to hear from you!<br>
        </div>
        <div class="infos">
              <div class="infotxt">
              <?php echo $page->textleft()->kirbytext()?>
              </div>
              <div class="infolink">
                    <?php echo $page->textright()->kirbytext()?>
                </div>
                     </div>
    </div>
</div>
