<form class="" action="" method="post">
  <input type="submit" name="submit" value="Pdf->Gen">
</form>
<?php

  require_once __DIR__ . '/vendor/autoload.php';
  //

$defaultConfig = (new Mpdf\Config\ConfigVariables())->getDefaults();
$fontDirs = $defaultConfig['fontDir'];

$defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
$fontData = $defaultFontConfig['fontdata'];

  $mpdf = new \Mpdf\Mpdf([
    'mode' => 'utf-8',
    'format' => 'A4-L',
    'margin_left' => 0,
    'margin_right' => 0,
    'margin_top' => 0,
    'margin_bottom' => 0,
    'margin_header' => 0,
    'margin_footer' => 0,
    'fontDir' => array_merge($fontDirs, [
       __DIR__ . '/vendor/mpdf/mpdf/ttfonts',
   ]),
   'fontdata' => $fontData + [
       'oswald' => [
         'R' => 'Oswald-Medium.ttf'
       ],
       'oswald-i'=>[
         'I' => 'Oswald-RegularItalic.ttf',
       ],
       'oswald-l'=>[
         'R' => 'Oswald-Light.ttf',
       ]
   ],
   'default_font' => 'oswald'
  ]);
  $mpdf->SetCompression(true);
  ob_start();
?>
<?php
  $projects = explode(",",$_GET["p"]);
  $avant = explode(",",$_GET["a"]);
  // $project = $site->page("projets")->find($projects[0]);
?>
<?php echo css("assets/css/print.css") ?>

  <div class="page">
    <div class="ouverture">
      <span class="sur">Bonjour Monde</span>
      <br>
      <h1>
        <?php echo $page->destinataire() ?>
        <br/>
        <?php echo $page->project() ?>
        <br/>
        <span><?php echo $page->date()?>
        </span>

      </h1>
    </div>
  </div>
  <div class="page">
    <div class="ouverture">
      <span class="sur">Bonjour Monde</span>
      <br><br>
        <div class="bjm">
          <?php echo $page->text() ?>
        </div>
      </div>
      </h1>
    </div>
  <?php if (isset($avant)): ?>
    <?php foreach($avant as $key => $value): ?>
      <?php $value = str_replace(' ', '', $value); ?>
      <?php $av = $site->page("projets")->find($value) ?>
      <div class="page nv">
        <h1><?php echo $av->title(); ?></h1><br>
        <div class="desc"><?php echo $av->text()->kt(); ?></div>
      </div>
      <?php foreach ($av->images() as $img): ?>
        <div class="page">
          <table style="width:100%">
            <tr>
              <th ><img class="gal-item" src="<?php echo $img->url() ?>" alt=""></th>
              <th class="caption"><?php echo $img->caption(); ?></th>
            </tr>
          </table>
        </div>
        <h6 class="bas">
          <span class="sur"><?php //echo $av->title(); ?></span>
          <!-- <span class="marge">&nbsp;&mdash;&nbsp;</span> -->
          <span class="marge"><?php //echo $av->type(); ?>,</span>
          <span class="marge"><?php //echo $av->place(); ?>,</span>
          <span class="marge"><?php //echo $av->city(); ?>,</span>
          <span class="marge"><?php //echo $av->year(); ?></span>
        </h6>
        <div class='break'></div>

      <?php endforeach; ?>

    <?php endforeach; ?>
  <?php endif; ?>

  <div class="page book">
    <h1>Bonjour Book</h1>
  </div>

  <?php foreach($projects as $key => $value): ?>
    <?php $value = str_replace(' ', '', $value); ?>
    <?php $project = $site->page("projets")->find($value) ?>
    <div class="page">
      <h1>
      <span class="sur"><?php echo $project->title(); ?></span><br>

      <span class="info">
      <?php if($project->type()->isNotEmpty()): ?>
      <?php echo $project->type(); ?>,
      <?php endif ?>
      <?php if($project->place()->isNotEmpty()): ?>
      <?php echo $project->place(); ?>,
      <?php endif ?>
      <?php if($project->city()->isNotEmpty()): ?>
      <?php echo $project->city(); ?>,
      <?php endif ?>
      <?php if($project->year()->isNotEmpty()): ?>
      <?php echo $project->year(); ?>
      <?php endif ?>
      </span>
      </h1>
    </div>
    <div class='break'></div>

    <div class="page">
      <img class="cover" src="<?php echo $project->images()->first()->url(); ?>" alt="">
      <div class="intro"><?php echo $project->text()->kirbytextRaw(); ?></div>
    </div>
    <h6 class="bas">
      <span class="sur"><?php echo $project->title(); ?></span>
      <span class="marge">&nbsp;&mdash;&nbsp;</span>
      <?php if($project->type()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->type(); ?>,</span>
      <?php endif ?>
      <?php if($project->place()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->place(); ?>,</span>
      <?php endif ?>
      <?php if($project->city()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->city(); ?>,</span>
      <?php endif ?>
      <?php if($project->year()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->year(); ?></span>
      <?php endif ?>
    </h6>
    <!-- breaking to the next project -->
    <div class='break'></div>

    <?php foreach ($project->images()->offset(1)->limit(4) as $img): ?>
        <div class="page gal">

          <table style="width:100%">
            <tr>
              <th ><img class="gal-item" src="<?php echo $img->url() ?>" alt=""></th>
              <th class="caption"><?php echo $img->caption(); ?></th>
            </tr>
          </table>

      </div>
      <h6 class="bas">
        <span class="sur"><?php echo $project->title(); ?></span>
        <span class="marge">&nbsp;&mdash;&nbsp;</span>
        <?php if($project->type()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->type(); ?>,</span>
      <?php endif ?>
      <?php if($project->place()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->place(); ?>,</span>
      <?php endif ?>
      <?php if($project->city()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->city(); ?>,</span>
      <?php endif ?>
      <?php if($project->year()->isNotEmpty()): ?>
      <span class="marge"><?php echo $project->year(); ?></span>
      <?php endif ?>
      </h6>
      <div class='break'></div>

    <?php endforeach; ?>



  <?php endforeach; ?>

<?php
  $html = ob_get_contents();
  ob_end_clean();

  $mpdf->WriteHTML($html);

  $mpdf->Output();
