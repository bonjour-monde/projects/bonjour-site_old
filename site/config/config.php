<?php

/*

---------------------------------------
License Setup
---------------------------------------

Please add your license key, which you've received
via email after purchasing Kirby on http://getkirby.com/buy

It is not permitted to run a public website without a
valid license key. Please read the End User License Agreement
for more information: http://getkirby.com/license

*/

c::set('license', 'K2-PERSONAL-ffd9710f533e85395ea36e3bcef7b14e');
c::set('debug', true);

/*

---------------------------------------
Kirby Configuration
---------------------------------------

By default you don't have to configure anything to
make Kirby work. For more fine-grained configuration
of the system, please check out http://getkirby.com/docs/advanced/options

*/

/* --------------------------------------------------
Gallery Grid settings

s : 50x50 px
m : 75x75 px (Kirby default)
l : 100x100 px
-------------------------------------------------- */

c::set("kirbyGalleryGrid", "m");

/* ---------------------------------------

 Custom panel stylesheet

 --------------------------------------- */

c::set('panel.stylesheet', 'assets/css/panel.css');
